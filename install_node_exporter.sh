#!/bin/bash

yum install wget -y

wget https://github.com/prometheus/node_exporter/releases/download/v1.0.1/node_exporter-1.0.1.linux-amd64.tar.gz

tar xzf node_exporter-1.0.1.linux-amd64.tar.gz
mv node_exporter-1.0.1.linux-amd64/node_exporter /usr/sbin/
chown root. /usr/sbin/node_exporter
rm -rf node_exporter-1.0.1.linux-amd64*
useradd -r -s /sbin/nologin node-exporter
mkdir -p /var/lib/node_exporter/textfile_collector
chown -R node-exporter. /var/lib/node_exporter
systemctl daemon-reload
systemctl start node-exporter
systemctl enable node-exporter