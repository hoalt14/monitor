# Monitoring Linux Server with Prometheus and Grafana

- <https://github.com/RedisTimeSeries/grafana-redis-datasource>

## STEP 1 - Install Prometheus

- ssh user@x.x.x.x
- mkdir -p prometheus/data
- cp [prometheus.yml](./prometheus.yml) prometheus/

```bash
docker run -d \
  --name prometheus-server \
  --network host \
  -u $(id -u):$(id -g) \
  -p 9090:9090 \
  -v /home/$USER/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml \
  -v /home/$USER/prometheus/data:/prometheus \
  prom/prometheus:v2.22.0
```

## STEP 2 - Install Grafana

- ssh user@x.x.x.x
- mkdir -p grafana/{data,log}

```bash
docker run -d \
  --name grafana-server \
  -u $(id -u):$(id -g) \
  -p 3000:3000 \
  -v /home/$USER/grafana/data:/var/lib/grafana \
  -v /home/$USER/grafana/log:/var/log/grafana \
  -e "GF_INSTALL_PLUGINS=redis-datasource" \
  grafana/grafana:7.3.1
```

- `http://x.x.x.x:3000` with account **`admin/admin`** -> **change password**
- `http://x.x.x.x:3000/datasources` to add data source

![screenshot](./img/grafana-datasource_01.png)

![screenshot](./img/grafana-datasource_02.png)

### example install plugin by docker command

`bash`: docker exec -it grafana-server grafana-cli plugins install redis-datasource

## STEP 3 - Install Node Exporter

- sudo cp [node_exporter](./node_exporter) /etc/sysconfig/
- sudo cp [node-exporter.service](./node-exporter.service) /usr/lib/systemd/system/
- sudo ./[install_node_exporter.sh](./install_node_exporter.sh)

## STEP 4 - Import dashboard on Grafana

- access <http://x.x.x.x:3000/dashboard/import>
- **`Node Exporter`** import with id **`11074`** or reference: <https://grafana.com/grafana/dashboards/11074>
- **`Redis`** import with id **`12776`** or reference: <https://grafana.com/grafana/dashboards/12776>
- **`Haproxy`** import with id **`12693`** or reference: <https://grafana.com/grafana/dashboards/12693>

![screenshot](./img/grafana-import.png)

## STEP 5 - Add node

- edit file [prometheus.yml](./prometheus.yml) to add node
- docker restart prometheus-server
